// suppress inspection "SpellCheckingInspection" for whole file

:fn-c-quote-prng-notice: footnote:[ \
    This is using a fast \
    link:https://en.wikipedia.org/wiki/Pseudorandom_number_generator[prng] \
    to choose the random quote. this means that if you only have a few quotes (<10) \
    you will see a lot of repitition \
]
