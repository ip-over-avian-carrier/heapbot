---
title: "Command Documentation for Heapbot"
author: "HeapUnderflow"
date: "2020-06-19 17:07:26"
lang: "en"
---

- **HINT**: Ids can be written with and without the `#` in front
- **HINT**: Required arguments are surrounded with `{}` optional ones with `[]`

# Plugin: Repeater

## Command: addpogey

| Type      | Description                           |
| --------- | ------------------------------------- |
| Name      | addpogey                              |
| Alias     | addrep                                |
| Level     | Moderator                             |
| Arguments | pogey ops (see: [Extra: Pogey Ops][]) |

Add a new pogey/repeatable to the bot

- **Usage**: `!!addpogey {pogey ops}`
- **Examples**:
  1. `!!addpogey target=deccCult`
  2. `!!addpogey t=deccWoah min=10 max=15`
  3. `!!addpogey t=a r=b`

## Command: delpogey

| Type      | Description   |
| --------- | ------------- |
| Name      | delpogey      |
| Alias     | delrep        |
| Level     | Moderator     |
| Arguments | pogey_trigger |

Remove a pogey/repeatable from the bot by its trigger.

**Note**: This does **not** reset its counter, see [Command: clearcount][]

- **Usage**: `!!delpogey {pogey_trigger}`
- **Examples**:
  1. `!!delpogey deccCult`

## Command: clearcount

| Type      | Description   |
| --------- | ------------- |
| Name      | clearcount    |
| Level     | Moderator     |
| Arguments | pogey_trigger |

Clear the counter of a pogey by its trigger.

- **Usage**: `!!clearcount {pogey_trigger}`
- **Examples**:
  1. `!!clearcount deccCult`

# Plugin: Quote

## Command: quote

| Type                | Description |
| ------------------- | ----------- |
| Name                | quote       |
| Level               | Anyone      |
| Argument (Optional) | quote_id    |

Show a quote either by its id, or if none is passed a random one.

- **Usage**: `!!quote [id]`
- **Examples**:
  1. `!!quote`
  2. `!!quote #abc1`

## Command: blamequote

| Type     | Description |
| -------- | ----------- |
| Name     | blamequote  |
| Level    | Anyone      |
| Argument | quote_id    |

See who created a given quote

- **Usage**: `!!blamequote {id}`
- **Examples**:
  1. `!!blamequote #abc1`

## Command: addquote

| Type     | Description |
| -------- | ----------- |
| Name     | addquote    |
| Level    | Moderator   |
| Argument | quote       |

Add a new quote

- **Usage**: `!!addquote {quote}`
- **Examples**:
  1. `!!addquote boxerboxer is a box hiding in a box`

## Command: delquote

| Type     | Description |
| -------- | ----------- |
| Name     | delquote    |
| Level    | Moderator   |
| Argument | quote_id    |

Remove a existing quote by its id

- **Usage**: `!!delquote {quote_id}`
- **Examples**:
  1. `!!delquote #abc1`

## Command: quote_game

| Type                | Description |
| ------------------- | ----------- |
| Name                | quote_game  |
| Level               | Moderator   |
| Argument            | quote_id    |
| Argument (Optional) | game        |

Set, Override or Remove the associated game of a quote

- **Usage**: `!!quote_game {quote_id} [game]`
- **Examples**:
  1. `!!quote_game #abc1 Hello Kitty Land`
  2. `!!quote_game #abc1`

# Plugin: Echo [flag: echo]

`-- [ only enabled with the echo flag ] --`

# Plugin: Custom

## Command: addcustom

| Type                | Description                                  |
| ------------------- | -------------------------------------------- |
| Name                | addcustom                                    |
| Alias               | addcom                                       |
| Level               | Moderator                                    |
| Argument            | name                                         |
| Argument (Optional) | options (see: [Extra: Custom Opts][])        |
| Argument            | command_text (see [Extra: Custom Command][]) |

Add a new custom command. **Note**: All command names are _literal_, any (or no) prefix can be chosen, even
if its best practice to use the same prefix for all of them (this means both `a` and `supersecretbot!!!???!!a` are
valid prefixes, in the examples `$` is chosen as a prefix.). The name **cannot** contain spaces, every other character is valid.

- **Usage**: `!!addcustom {name} [options] {command_text}`
- **Examples**:
  1. `!!addcustom $yes no`
  2. `!!addcustom $slap {caller} slaps {arg(1)} with a fish!`
  3. `!!addcustom $so -p=mod Hey! Listen! Look at the cool person {arg(1)} https://twitch.tv/{arg(1)}`
- **Known Bugs**:
  - Even though you can escape an replacable argument with `\`, it does currently not work due to a weakness
    in the parser itself.

## Command: delcustom

| Type     | Description |
| -------- | ----------- |
| Name     | delcustom   |
| Alias    | delcom      |
| Level    | Moderator   |
| Argument | name        |

Remove a existing custom command

- **Usage**: `!!deccustom {name}`
- **Examples**:
  1. `!!delustom $slap`
  
# Plugin: Auto Shoutout

**NOTE**: All "commands" under this use the common prefix `auto_shoutout` (the examples will reflect this)

## Command: add

| Type     | Description |
| ----     | ----------- |
| Name     | add         |
| Alias    | create      |
| Level    | Moderator   |
| Argument | username    |
| Argument | message     |

Add a new automatic shoutout

- **Usage**: `!!auto_shoutout add {username} {message}`
- **Examples**: 
  1. `!!auto_shoutout add god_gamer69420 Yo peeps, check out the amazing god_gamer69420 at https://twitch.tv/god_gamer69420`

## Command remove

| Type     | Description |
|----------|-------------|
| Name     | remove      |
| Alias    | delete      |
| Alias    | rm          |
| Level    | Moderator   |
| Argument | username    |

Remove autoshoutout

- **Usage**: `!!auto_shoutout remove {username}`
- **Examples**:
  1. `!!auto_shoutout remove god_gamer69420`

## Command list

| Type  | Description |
|-------|-------------|
| Name  | list        |
| Level | Moderator   |

List all the auto shoutouts configured

- **Usage**: `!!auto_shoutout list`
- **Examples**:
  1. `!!auto_shoutout list**

## Command inspect

**NOTE**: unimplemented

# Extra: Pogey Ops

For specifying pogeys all ops are in the form of `op=value` and are separated by spaces.
There is no escaping, so values cannot have spaces.

**Example**: `trigger=test min=2 max=5`

| Op       | Required | Descrioption                                 | Default value      |
| -------- | -------- | -------------------------------------------- | ------------------ |
| trigger  | Yes      | The trigger for the given Pogey              |                    |
| t        | Yes      | Alias for trigger                            |                    |
| response | No       | Response that is repeated                    | Value of [trigger] |
| r        | No       | Alias for response                           | Value of [trigger] |
| min      | No       | Minimum amount of repetitions for [response] | 1                  |
| l        | No       | Alias for min                                | 1                  |
| max      | No       | Maximum amount of repetitions for [response] | 5                  |
| h        | No       | Alias for max                                | 5                  |

- **Note**: If a alias is given, the normal name doesnt have to be used
- **Note**: Arguments can be specified multiple times, later occurances will overide earlier ones

# Extra: Custom Opts

All custom commands can be restricted to an access level. for that the following options can be passed.
**Note**: Note that lower permission levels are inclusive, which means as an example: a command executable by a moderator can be executed by broadcaster too.

| Argument            | Description                                                                                                                                                                                  |
| ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| -p=[level]          | Set the required permission level to [level], valid values are: `subscriber` (`sub`), `moderator` (`mod`), `broadcaster` (`brd`). Values in `()` are short forms and can be used as an alias |
| -subscriber (-sub)  | Alias for `-p=subscriber`                                                                                                                                                                    |
| -moderator (-mod)   | Alias for `-p=moderator`                                                                                                                                                                     |
| -broadcaster (-brd) | Alias for `-p=broadcaster`                                                                                                                                                                   |

Be aware that the permission level of a command _currently_ does **not** govern editing permissions! A command
limited to broadcaster can very well be removed and readded by a mod with different permissions.

# Extra: Custom Command

A custom command can contain several different 'replacables' in addition to normal text.
These replacables are surrounded by `{}`, if you want to use a `{` without starting a replacable you can prefix it with
a backslash like this `\{`, which will make the parser ignore it. A replacable can have arguments. Arguments are always enclosed in `()`. Spaces within do not matter.

Valid replacables are:

| Name   | Arguments | Description                                                                                                                                                        |
| ------ | --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| caller | -         | The username of whoever calls the command                                                                                                                          |
| arg    | {idx}     | An argument given to the command. idx specifies the command index, `arg(1)` is the first, `arg(2)` the second agument and so on. `arg(0)` is a alias for `caller`  |
| time   | [offset]  | The current time. The optional argument offset specifies the offset from utc in hours. `time` means 'current time utc', `time(-2)` means 'current time utc - 2hrs' |
