#!/bin/sh

die (){
	printf "\e[1m\e[31mERROR\e[0m: %s\n" "$@"
	exit 1
}

hascom (){
	type "$1" >/dev/null 2>&1 || die "Requiring $1 but it doesnt exist..."
}

hascom "mkdir"
hascom "pandoc"

BASEDIR="../target/comdoc"

if [ ! -d "$BASEDIR" ]; then
	mkdir -p "$BASEDIR"
else
	rm -Rf "$BASEDIR"
	mkdir -p "$BASEDIR"
fi



		# --template=template.html \
pandoc \
	-f markdown \
	-t html \
	--standalone \
	--toc \
	--toc-depth=2 \
	--number-sections \
	--eol=lf \
	--self-contained \
	--email-obfuscation=javascript \
	-c "doc.css" \
	"doc.md" \
	-o "$BASEDIR/documentation.html"
