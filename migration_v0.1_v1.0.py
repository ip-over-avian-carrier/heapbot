#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## This will migrate the data from the 0.1 version to the 1.0 version of the bot

import json
from pathlib import Path

CONFIG = "config.json"
CC = "cc.json"
POGEYS = "pogeys.json"
QUOTES = "quotes.json"
STATE = "state.json"

DB_BASE = Path("database/")

def strip_chn(chn: str) -> str:
    if len(chn) > 0 and chn[0] == '#':
        return chn[1:]
    else:
        return chn

def check_db(usr: str):
    """Checks if the directory for a user exists, and if not creates it"""
    P = DB_BASE.joinpath(usr)

    if not P.exists():
        P.mkdir(parents=True, exist_ok=True)

def migrate_config():
    """This will migrate the configuration file to the new config.yml"""
    with open(CONFIG, "r", encoding="utf-8") as cfg:
        data = json.load(cfg)

    with open("config.yml", "w", encoding="utf-8") as config:
        config.write("---\n");
        config.write("bot:\n");
        config.write("  user: \"{}\"\n".format(data["bot"]["user"]))
        config.write("  token: \"{}\"\n".format(data["bot"]["oauth"]))
        config.write("channels:\n")
        for tgt in data["targets"]:
            config.write("  - \"{}\"\n".format(tgt))

def migrate_quotes():
    """This will migrate the quotes list to the per-channel quotes.json"""
    with open(QUOTES, "r", encoding="utf-8") as cfg:
        data = json.load(cfg)

    for chan, val in data.items():
        check_db(chan)
        new = {"quotes": {}}
        for k, v in val["quotes"].items():
            new["quotes"][k] = {
                "text": v["text"],
                "creator": v["quoter"],
                "game": None,
                "time": v["time"]
            }

        with open(DB_BASE.joinpath(strip_chn(chan), "quotes.json"), "w", encoding="utf-8") as w:
            json.dump(new, w)

def migrate_pogeys():
    """This will migrate the pogey list to the per-channel repeater.json"""
    with open(POGEYS, "r", encoding="utf-8") as cfg:
        data = json.load(cfg)

    for chan, val in data.items():
        check_db(chan)
        new = {"reps": [], "counters": {}, "no": False}

        for entry in val:
            if entry["start"] < 1:
                entry["start"] = 1

            if entry["max"] < entry["start"]:
                entry["max"] = entry["start"]

            new["reps"].append({
                "trigger": entry["trigger"],
                "response": entry["response"],
                "min": entry["start"],
                "max": entry["max"] 
            })

        with open(DB_BASE.joinpath(strip_chn(chan), "repeatable.json"), "w", encoding="utf-8") as w:
            json.dump(new, w)

def migrate_state():
    """This will migrate the state list to the per-channel repeater.json"""
    with open(STATE, "r", encoding="utf-8") as cfg:
        data = json.load(cfg)

    for chan, val in data.items():
        # general settings
        with open(DB_BASE.joinpath(strip_chn(chan), "GENERAL_SETTINGS.json"), "w", encoding="utf-8") as w:
            json.dump({"flags": {"bits": 0b100000000 if val["flags"]["bits"] & 0b1 == 0b1 else 0b0 }}, w)

        # counters
        with open(DB_BASE.joinpath(strip_chn(chan), "repeatable.json"), "r", encoding="utf-8") as migrated:
            rmig = json.load(migrated)

        for n, c in val["counters"].items():
            rmig["counters"][n] = c

        with open(DB_BASE.joinpath(strip_chn(chan), "repeatable.json"), "w", encoding="utf-8") as w:
            json.dump(rmig, w)


migrate_config()
migrate_quotes()
migrate_pogeys()
migrate_state()
