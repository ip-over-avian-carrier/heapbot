/// Used for errors that do not need to be handled, but should not be silenced
macro_rules! err_log {
	($e:expr) => {{
		if let Err(why) = $e {
			tracing::error!("encountered error: {:?}", why);
			}
		}};
}

pub mod bot;
pub mod config;
pub mod db;
pub mod model;
pub mod parser;
pub mod utils;

macro_rules! try_error {
	($ex:expr $(, $msg:expr)?) => {{
		match $ex {
			Ok(value) => value,
			Err(why) => {
				tracing::error!(error=?why, $(message=?$msg, )? "programm error");
				return;
			}
		}
	}}
}

/// Entry point, see [`realmain`] for the real entry point
fn main() {
	let mut rt = tokio::runtime::Builder::new()
		.basic_scheduler()
		.threaded_scheduler()
		.enable_all()
		.build()
		.expect("failed to build runtime");

	// We are manually constructin the runtime here,
	// so we can pass a handle to it into main
	// which then can be used on other threads to queue tasks onto the scheduler
	let handle = rt.handle().clone();
	rt.block_on(async { realmain(handle).await });
}

/// The real entrypoint of the bot
async fn realmain(_rt: tokio::runtime::Handle) {
	tracing_subscriber::fmt::init();
	let cfg =
		match try_error!(config::Cfg::load_or_create("config.toml").await, "failed to load config")
		{
			Some(cfg) => cfg,
			None => {
				tracing::error!(
					"missing config. a new one was created, please fill it out and start again"
				);
				return;
			}
		};

	let mut db = db::DB::new(std::path::Path::new("database/").to_path_buf());
	try_error!(db.init().await);

	let mut conninfo = model::TIRCConnectionInfo::default();
	conninfo.user = cfg.bot.user;
	conninfo.oauth = cfg.bot.token;

	let mut bot = bot::TwitchBot::new(
		db,
		_rt,
		conninfo,
		cfg.bot
			.channels
			.iter()
			.filter_map(|v| match model::Channel::new(v) {
				Some(v) => Some(v),
				None => {
					tracing::warn!("{} is not a valid channel, ignoring", v);
					None
				}
			})
			.collect::<Vec<_>>(),
		cfg.app.clone(),
	);

	#[cfg(feature = "msg-trace")]
	bot.register_plugin(Box::new(crate::bot::plugins::EchoPlugin));
	bot.register_plugin(Box::new(crate::bot::plugins::QuotePlugin::default()));
	bot.register_plugin(Box::new(crate::bot::plugins::RepeaterPlugin::default()));
	bot.register_plugin(Box::new(crate::bot::plugins::SnugTrollPlug::new()));
	bot.register_plugin(Box::new(crate::bot::plugins::CustomCommandPlugin::default()));
	bot.register_plugin(Box::new(crate::bot::plugins::AutoShoutoutPlug::default()));
	bot.run().await;
}
