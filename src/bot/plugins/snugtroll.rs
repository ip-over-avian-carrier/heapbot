use super::prelude::*;
use once_cell::sync::Lazy;
use rand::Rng;
use regex::Regex;
use std::collections::HashMap;
use tokio::time::{Duration, Instant};

static MESSAGES: [&str; 7] = [
	"snug deccCult",
	"snug deccHiYo",
	"snug deccWoah",
	"snug deccWow",
	"hi snug deccHiYo",
	"hi snug deccHIYo",
	"hello snug deccWoah",
];

static ANSWER_REGEX: Lazy<Regex> = Lazy::new(|| {
	regex::RegexBuilder::new(r"^(?:ha?i)? *hea?p *(?:\w+)?$")
		.case_insensitive(true)
		.build()
		.unwrap()
});

#[derive(Debug, Default, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct SnugState {
	off: bool,
	count: u32,
}

pub struct SnugTrollPlug {
	last_seen_snug: Instant,
	has_answered: bool,
	states: HashMap<Channel, SnugState>,
}

impl SnugTrollPlug {
	/// This is equivalent to calling default
	pub fn new() -> SnugTrollPlug {
		Self::default()
	}
}

impl Default for SnugTrollPlug {
	fn default() -> Self {
		SnugTrollPlug {
			last_seen_snug: Instant::now() - Duration::from_secs(60 * 60 + 1),
			has_answered: false,
			states: HashMap::new(),
		}
	}
}

#[async_trait::async_trait]
impl Plugin for SnugTrollPlug {
	async fn init(&mut self, s: &mut State) -> Result<(), String> {
		self.states = match s.db.load_known(self).await {
			Ok(states) => states,
			Err(error) => return Err(format!("{:?}", error)),
		};

		s.register_command(self, "snug");
		s.register_command(self, "snugbait");
		Ok(())
	}
	async fn on_message(&mut self, state: &mut State, m: &Message) -> PlugResult {
		if self.states.get(&m.target).map(|st| st.off).unwrap_or(false) || m.name != "snugflag" {
			return PlugResult::Continue;
		}

		let now = Instant::now();

		if !self.has_answered
			&& self.last_seen_snug > now - Duration::from_secs(5 * 60)
			&& ANSWER_REGEX.is_match(&m.content)
		{
			tracing::info!("snug was jebaited :)");
			let cnt = {
				let entry = self.states.entry(m.target.clone()).or_default();
				entry.count += 1;
				entry.count
			};

			err_log!(m.respond(format!("jebaited Jebaited (count = {})", cnt)).send(state).await);
			self.has_answered = true;

			// we can unwrap here as we are shure to default it up at the entry retrieval
			err_log!(state.db.store(self, &m.target, &self.states.get(&m.target).unwrap()).await);
		} else if now - self.last_seen_snug > Duration::from_secs(60 * 60)
			&& rand::thread_rng().gen::<bool>()
		{
			let message = &MESSAGES[rand::thread_rng().gen_range(0, MESSAGES.len())];
			tracing::info!(jebait=?message, "found snug after an hour of silence");
			tokio::time::delay_for(Duration::from_millis(1657)).await;
			err_log!(m.respond(String::from(*message)).send(state).await);
			self.has_answered = false;
		}

		self.last_seen_snug = now;

		PlugResult::Stop
	}

	async fn on_command(&mut self, s: &mut State, m: &Message, cn: &str, _: &str) -> PlugResult {
		if !m.flags.is_high_privileged() {
			return PlugResult::Stop;
		}

		let dirty = if cn == "snug" {
			let state = self.states.entry(m.target.clone()).or_default();
			state.off = !state.off;
			tracing::info!(state=%if state.off { "off" } else { "on" }, "snug state changed");
			if state.off {
				err_log!(m.respond(String::from("snug is now off FeelsBadMan")).send(s).await);
			} else {
				err_log!(m.respond(String::from("snug is now on heheheh deccCool")).send(s).await);
			}

			true
		} else {
			false
		};

		if dirty {
			err_log!(s.db.store(self, &m.target, &self.states.get(&m.target).unwrap()).await);
		}

		PlugResult::Stop
	}

	fn priority(&self) -> usize {
		101
	}
	fn name(&self) -> &'static str {
		"snug_troll"
	}
}
