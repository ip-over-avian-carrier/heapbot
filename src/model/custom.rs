use crate::{model::Message, parser::custom::ParseError};
use chrono::{DateTime, FixedOffset, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Expandable {
	items: Vec<ExpandItem>,
}

impl Expandable {
	pub fn parse(s: &str) -> Result<Expandable, ParseError> {
		if s.is_empty() {
			return Err(ParseError::Empty);
		}
		// Delegate parsing to the pest parser
		crate::parser::custom::parse(s)
	}

	pub fn from_parts(parts: Vec<ExpandItem>) -> Expandable {
		Expandable { items: parts }
	}

	pub fn expand(&self, message: &Message, args: &[&str]) -> Result<String, ExpansionError> {
		Expander::expand(self, message, args)
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "t", rename = "snake_case")]
pub enum ExpandItem {
	Text { content: String },
	Time { zone: Option<i32> },
	Caller,
	Arg { idx: usize },
}

#[derive(Clone, Copy, Debug)]
pub struct ExpandArgs<'l> {
	message: &'l Message,
}

pub struct Expander;

impl Expander {
	fn expand(exp: &Expandable, msg: &Message, args: &[&str]) -> Result<String, ExpansionError> {
		let mut out = String::new();
		for item in &exp.items {
			match item {
				ExpandItem::Text { content } => out.push_str(&content),
				ExpandItem::Arg { idx } => {
					if *idx == 0 {
						out.push_str(&msg.name);
						continue;
					}

					let idx = idx - 1;
					if idx >= args.len() {
						return Err(ExpansionError::MissingArg { idx: idx + 1 });
					}

					out.push_str(&args[idx]);
				}
				ExpandItem::Caller => out.push_str(&msg.name),
				ExpandItem::Time { zone: Some(offset) } => {
					let now = Utc::now();
					out.push_str(
						&now.with_timezone(&FixedOffset::east(*offset * 3600))
							.format("%T")
							.to_string(),
					);
				}
				ExpandItem::Time { zone: None } => {
					let now = Utc::now();
					out.push_str(&now.format("%T").to_string());
				}
			}
		}
		Ok(out)
	}
}

#[derive(Debug, thiserror::Error)]
pub enum ExpansionError {
	#[error("you need to pass at least one more argument (index = {})", .idx)]
	MissingArg { idx: usize },
}
