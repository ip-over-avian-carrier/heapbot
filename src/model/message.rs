use crate::model::{Channel, ChannelId, MessageFlags, MessageId, UserId};
use irc::proto::{
	command::Command as IrcCommand,
	message::{Message as IrcMessage, Tag},
};
use once_cell::sync::Lazy;
use regex::Regex;
use std::collections::HashMap;

static OWNER: &str = "heapunderflow";
static MOJO: &str = "buymymyojo";

/// A parsed twitch message. It closely mirrors the format of the actual twitch
/// object
#[derive(Debug, Clone)]
pub struct Message {
	pub bare: bool,

	// ids
	/// The message-id
	pub id: MessageId,
	/// The user-id
	pub user_id: UserId,
	/// The channel-id
	pub room_id: ChannelId,

	// important metadata
	/// Flags set for the message (see
	/// [MessageFlags](./struct.MessageFlags.html))
	pub flags: MessageFlags,
	/// Bits for the message
	pub bits: Option<u32>,
	/// User badges
	pub badges: HashMap<String, u8>,
	/// The original source of the message
	pub target: Channel,

	// misc metadata

	// we parse the username from the irc message name
	// it is always in the form of <username>!<username>@<username>.tmi.twitch.tv
	/// The username of the user
	pub name: String,
	/// Display name of the user
	pub display_name: Option<String>,
	/// Color of the user
	pub color: Option<String>,
	/// Timestamp the twitch server recieved the message
	pub tmi_sent_ts: String,

	// content
	/// Content of the message
	pub content: String,

	// extra stuff
	/// Extra Tags. This is a list of unknown tags and there is no gurante about
	/// this lists content
	pub extra_tags: HashMap<String, Option<String>>,
}

impl Message {
	/// Parse a IrcMessage into a Message
	pub fn parse_irc(mesg: IrcMessage) -> Option<Message> {
		static TMI_REG: Lazy<Regex> = Lazy::new(|| {
			match Regex::new(
				r#"^:?([a-zA-Z0-9_]+)!([a-zA-Z0-9_]+)@([a-zA-Z0-9_]+)\.tmi\.twitch\.tv$"#,
			) {
				Ok(reg) => reg,
				Err(why) => {
					tracing::error!("fatal error while compiling regex: {:?}", why);
					panic!("failed to compile regex: {:?}", why);
				}
			}
		});

		let span = tracing::trace_span!("message_parse");
		let _guard = span.enter();
		// check if we have a PRIVMSG, otherwise abort
		let (target, content) = match mesg.command {
			IrcCommand::PRIVMSG(target, content) => (target, content),
			_ => return None,
		};

		tracing::trace!("we have privmsg");

		// create a new message object here (will be populated later this function)
		let mut new = Message {
			bare: false,

			id: MessageId::default(),
			user_id: UserId::default(),
			room_id: ChannelId::default(),

			flags: MessageFlags::default(),
			bits: None,
			badges: HashMap::new(),
			// already retrieved above
			target: Channel::new(&target).unwrap(),

			name: String::new(),
			display_name: None,
			color: None,
			tmi_sent_ts: String::new(),
			// already retrieved above
			content,

			extra_tags: HashMap::default(),
		};

		// retrieve the username from the prefix
		match mesg.prefix.as_ref().and_then(|e| TMI_REG.captures(e)).and_then(|cap| cap.get(2)) {
			Some(username) => new.name = username.as_str().to_owned(),
			None => return None,
		};

		tracing::trace!(username=%new.name);

		if new.name == OWNER {
			new.flags |= MessageFlags::OWNER;
		}

		if new.name == MOJO {
			new.flags |= MessageFlags::MOJO;
		}
		let tags = match mesg.tags {
			Some(tags) => tags,
			None => {
				// if we do not have tags, mark the message as bare and return
				tracing::trace!("new message is bare message");
				new.bare = true;
				return Some(new);
			}
		};

		Self::parse_tags(&mut new, tags);

		tracing::trace!(id=?new.id, "parsed message");

		Some(new)
	}

	/// Parse the tags out of the message
	fn parse_tags(new: &mut Message, tgs: Vec<Tag>) {
		let span = tracing::trace_span!("tags");
		let _guard = span.enter();

		for Tag(k, v) in tgs {
			if v.is_none() {
				continue;
			}

			let val = v.unwrap();
			match k.as_str() {
				"badges" => {
					for badge in val.split(',') {
						let parts = badge.split('/').collect::<Vec<_>>();
						if parts.len() < 2 {
							new.badges.insert(parts[0].to_owned(), 0);
						} else {
							new.badges.insert(parts[0].to_owned(), parts[1].parse().unwrap_or(0));
						}

						match parts[0] {
							"moderator" => new.flags |= MessageFlags::MODERATOR,
							"broadcaster" => new.flags |= MessageFlags::BROADCASTER,
							"subscriber" => new.flags |= MessageFlags::SUBSCRIBER,
							_ => (),
						}
					}
				}
				"bits" => {
					new.bits = Some(val.parse().unwrap_or(0));
					new.flags |= MessageFlags::BITS;
				}
				"color" if !val.is_empty() => {
					new.color = Some(val);
				}
				"display-name" if !val.is_empty() => {
					new.display_name = Some(val);
				}
				"id" => {
					new.id = val.into();
				}
				"room-id" => {
					new.room_id = val.into();
				}
				"tmi-sent-ts" => {
					new.tmi_sent_ts = val;
				}
				"user-id" => {
					new.user_id = val.into();
				}
				tg => {
					tracing::trace!(tag=%tg, val=%val, "unknown tag");
					new.extra_tags
						.insert(String::from(tg), if val.is_empty() { None } else { Some(val) });
				}
			}
		}
	}

	pub fn respond(&self, message: impl Into<String>) -> Response {
		Response { target: self.target.clone(), message: message.into() }
	}
}

use crate::model::State;
use tokio::sync::mpsc::error::SendError;
#[derive(Debug, Clone)]
pub struct Response {
	pub target: Channel,
	pub message: String,
}

impl Response {
	pub async fn send(self, ctx: &mut State) -> Result<bool, SendError<Self>> {
		ctx.send(self).await
	}
}
