use crate::model::State;
use reqwest::Client;
use serde::Deserialize;
use std::{fmt, sync::Arc};
use tokio::{
	sync::Mutex,
	time::{Duration, Instant},
};

#[derive(Debug, Default, Eq, PartialEq)]
pub struct TwitchApiMarker;
impl typemap::Key for TwitchApiMarker {
	type Value = Arc<Mutex<TwitchApi>>;
}

pub struct TwitchApi {
	bucket_info: (usize, Duration),
	bucket: usize,
	reset_at: Instant,
	client_id: String,
	client_secret: String,
	oauth: Option<OauthInfo>,
}

impl TwitchApi {
	pub fn new(cid: &str, csc: &str, bucket_max: usize, bucket_reset_ms: u64) -> TwitchApi {
		TwitchApi {
			bucket_info: (bucket_max, Duration::from_millis(bucket_reset_ms)),
			bucket: bucket_max,
			reset_at: Instant::now() + Duration::from_millis(bucket_reset_ms),
			client_id: String::from(cid),
			client_secret: String::from(csc),
			oauth: None,
		}
	}

	async fn _query_new_oauth(&self, client: &Client) -> Result<OauthInfo, reqwest::Error> {
		static ENDPOINT: &str = "https://id.twitch.tv/oauth2/token";

		let span = tracing::debug_span!("_query_new_oauth");
		let _guard = span.enter();

		let resp = client
			.post(ENDPOINT)
			.query(&[
				("client_id", self.client_id.as_str()),
				("client_secret", self.client_secret.as_str()),
				("grant_type", "client_credentials"),
			])
			.send()
			.await?
			.error_for_status()?;

		let rsp: TwitchTokenResponse = match resp.json().await {
			Ok(data) => data,
			Err(why) => {
				tracing::trace!(why=?why, "failed to unpack json from request");
				return Err(why);
			}
		};
		Ok(OauthInfo {
			token: rsp.access_token,
			expires: Instant::now() + Duration::from_secs(rsp.expires_in.saturating_sub(60)),
		})
	}

	async fn get_oauth(&mut self, client: &Client) -> Result<String, reqwest::Error> {
		let span = tracing::info_span!("get_oauth");
		let _guard = span.enter();

		if let Some(info) = &mut self.oauth {
			if Instant::now() < info.expires {
				tracing::debug!("oauth expired, re-fetching");
				let new = self._query_new_oauth(client).await?;
				self.oauth = Some(new);
				tracing::trace!("token found");
			}
		} else {
			tracing::debug!("no oauth, requesting");
			let new = self._query_new_oauth(client).await?;
			self.oauth = Some(new);
			tracing::trace!("token found");
		}

		Ok(self.oauth.clone().unwrap().token)
	}

	/// Easy ticket system
	async fn get_ticket(&mut self) {
		let span = tracing::debug_span!("get_ticket");
		let _guard = span.enter();

		let now = Instant::now();
		if self.reset_at < now {
			tracing::debug!(b=%self.bucket_info.0, "bucket reset");
			self.reset_at = now + self.bucket_info.1;
			self.bucket = self.bucket_info.0
		}

		if self.bucket == 0 {
			tracing::debug!("bucket ran out, waiting remaining time");
			tokio::time::delay_until(self.reset_at + Duration::from_millis(10)).await;
			self.reset_at = now + self.bucket_info.1;
			self.bucket = self.bucket_info.0 - 1;
			return;
		}

		self.bucket -= 1;
		tracing::debug!(c=%self.bucket, "ticket granted");
	}

	/// Get the current info about a stream
	pub async fn get_stream_info(
		&mut self,
		client: &Client,
		ids: &[&str],
		names: &[&str],
	) -> Result<ApiResponse<Stream>, reqwest::Error> {
		static ENDPOINT: &str = "https://api.twitch.tv/helix/streams";

		let span = tracing::info_span!("get_stream_info");
		let _guard = span.enter();

		tracing::trace!(idc=%ids.len(), namec=%names.len());

		self.get_ticket().await;

		let client_id = self.client_id.clone();
		let access_token = self.get_oauth(client).await?;

		let rsp: ApiResponse<Stream> = client
			.get(ENDPOINT)
			.header("client-id", &client_id)
			.bearer_auth(access_token)
			.query(
				&ids.iter()
					.map(|el| ("user_id", el))
					.chain(names.iter().map(|el| ("user_login", el)))
					.collect::<Vec<_>>(),
			)
			.send()
			.await?
			.error_for_status()?
			.json()
			.await?;

		tracing::trace!(c=%rsp.data.len(), "data len");
		Ok(rsp)
	}

	/// Get the name of a given game id
	pub async fn get_game_info(
		&mut self,
		state: &mut State,
		gameid: &str,
	) -> Result<Option<String>, reqwest::Error> {
		static ENDPOINT: &str = "https://api.twitch.tv/helix/games";

		let span = tracing::info_span!("get_game_info");
		let _guard = span.enter();

		tracing::trace!(gameid=%gameid);

		if let Some(name) = state.kv("twitch_api_game").find(gameid).await {
			return Ok(Some(name));
		}

		self.get_ticket().await;

		let http = state.http();
		let client_id = self.client_id.clone();
		let access_token = self.get_oauth(&http).await?;

		let mut gamedata: ApiResponse<Game> = http
			.get(ENDPOINT)
			.header("client-id", &client_id)
			.bearer_auth(access_token)
			.query(&[("id", &gameid)])
			.send()
			.await?
			.error_for_status()?
			.json()
			.await?;

		if gamedata.data.is_empty() {
			return Ok(None);
		}

		state.kv("twitch_api_game").add(gameid, &gamedata.data[0].name).await;
		tracing::trace!(gameid=%gameid, name=%&gamedata.data[0].name);
		Ok(Some(std::mem::take(&mut gamedata.data[0].name)))
	}
}

#[derive(Debug, Deserialize)]
pub struct ApiResponse<T>
where
	T: std::fmt::Debug,
{
	pub data: Vec<T>,
}

#[derive(Debug, Deserialize)]
pub struct Stream {
	pub game_id: String,
	pub id: String,
	pub language: String,
	pub started_at: String,
	pub thumbnail_url: String,
	pub title: String,
	#[serde(rename = "type")]
	pub status: String,
	pub user_id: String,
	pub user_name: String,
	pub viewer_count: u32,
}

#[derive(Debug, Deserialize)]
pub struct Game {
	pub id: String,
	pub name: String,
}

#[derive(Clone)]
struct OauthInfo {
	pub token: String,
	pub expires: Instant,
}

#[derive(Debug, Deserialize)]
struct TwitchTokenResponse {
	pub access_token: String,
	pub expires_in: u64,
	pub token_type: String,
}

impl fmt::Debug for OauthInfo {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.debug_struct("OauthInfo")
			.field("token", &crate::utils::elipsis(&self.token, 4))
			.field("expires", &format!("{:?}", self.expires))
			.finish()
	}
}
