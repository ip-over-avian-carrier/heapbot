//! Models used for the bot

use std::fmt;

/// Represents a result returned by a plugin's
/// on_* functions
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum PlugResult {
	/// Plugins with a lower priority are still called
	Continue,
	/// No further plugins will be called
	Stop,
	/// An internall error occured during the call that should be logged and
	/// displayed to the user (note: there is a size limitation of `512 -
	/// (command_name.len() + 2)` characters)
	Error {
		/// The error message to log and return to the user
		message: String,
		/// When true, the error will also be displayed to the user (see size
		/// limit above)
		report: bool,
	},
}

#[allow(dead_code)]
impl PlugResult {
	pub fn is_continue(&self) -> bool {
		matches!(self, PlugResult::Continue)
	}
	pub fn is_stop(&self) -> bool {
		matches!(self, PlugResult::Stop)
	}
	pub fn is_error(&self) -> bool {
		matches!(self, PlugResult::Error { .. })
	}
	pub fn new_error(&self, m: &str, r: bool) -> PlugResult {
		PlugResult::Error { message: String::from(m), report: r }
	}
}

/// Represents a single command
#[derive(Clone, Hash, Eq, PartialEq)]
pub struct Command {
	/// Case sensitive name of the command
	name: String,
	/// Case sensitive namespace of the command. Used for ordering and
	/// batch-removal. Should be the same as the plugin registering the command.
	namespace: String,
}

impl Command {
	/// Create a new command
	pub fn new(name: &str, namespace: &str) -> Command {
		Command { name: String::from(name), namespace: String::from(namespace) }
	}

	/// Name of the command
	pub fn name(&self) -> &str {
		&self.name
	}

	/// Namespace of the command
	pub fn ns(&self) -> &str {
		&self.namespace
	}
}

impl fmt::Debug for Command {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		f.debug_struct("Command")
			.field("name", &self.name)
			.field("namespace", &self.namespace)
			.finish()
	}
}

/// Info about the current user of the bot
#[derive(Debug)]
pub struct CurrentUser {
	pub name: String,
}

impl CurrentUser {
	pub fn new(n: &str) -> CurrentUser {
		CurrentUser { name: String::from(n) }
	}
}
