//! A repeatable, the actual reason this bot exists

use serde::{Deserialize, Serialize};

/// A single repeatable
#[derive(Debug, Serialize, Deserialize)]
pub struct Repeatable {
	/// Trigger text for the repeatable
	trigger: String,
	/// A single response item
	response: String,
	/// The minimum amount a response item is repeated (should be >0)
	min: u8,
	/// The maximum amount a response item is repeated (should be <256).
	/// beware that 256 may easily overflow the size limit for twitch chat.
	max: u8,
	/// The current count of repetitions, this will not be saved
	#[serde(skip_serializing, default = "default_current_repeatable")]
	current: usize,
}

impl Repeatable {
	pub fn new(trigger: String, response: String, min: u8, max: u8) -> Repeatable {
		Repeatable { trigger, response, min, max, current: min as usize }
	}

	/// Parse a new repeatable from the given string
	/// this will stringify all structured errors for easier
	/// consumption by the irc functions
	pub fn parse(s: &str) -> Result<Repeatable, String> {
		if s.is_empty() {
			return Err(String::from("you cannot use no options"));
		}
		use pest::error::ErrorVariant as EV;
		match crate::parser::repeatable::parse(s) {
			Ok(Ok(r)) => Ok(r),
			Ok(Err(e)) => Err(e),
			Err(e) => match e.variant {
				EV::CustomError { message } => Err(format!(
					"{} ({})",
					message,
					crate::utils::pest_location_to_string(e.location)
				)),
				EV::ParsingError { positives: pos, .. } => Err(format!(
					"expected rule {:?} ({})",
					pos[0],
					crate::utils::pest_location_to_string(e.location)
				)),
			},
		}
	}

	// TODO: Return a opaque object that implements display and so on,
	// to fix the issue of the off-by-one current counter in the command itself
	/// Build the repeatable to a string, then increase the counter
	pub fn build(&mut self) -> String {
		let count = self.current;
		self.current += 1;
		if self.current > self.max as usize {
			self.current = self.min as usize;
		}

		let mut out = String::with_capacity(self.response.len() * count + count);

		for _ in 0..count.saturating_sub(1) {
			out.push_str(&self.response);
			out.push(' ');
		}

		if count > 0 {
			out.push_str(&self.response);
		}

		out
	}

	/// Reset the counter of the repeatable to min, without considering its
	/// state
	pub fn reset(&mut self) {
		self.current = self.min as usize;
	}

	/// Trigger of the repeatable
	pub fn trigger(&self) -> &str {
		&self.trigger
	}

	/// Response item of the repeatable (this is repeated according to the
	/// current state)
	pub fn response(&self) -> &str {
		&self.response
	}

	/// Minimum amount of repetitions for the response item
	pub fn min(&self) -> u8 {
		self.min
	}

	/// Maximum amount of repetitions for the response item
	pub fn max(&self) -> u8 {
		self.max
	}

	/// The current state of the repetitions
	pub fn current(&self) -> usize {
		self.current
	}
}

/// The default value for the current repeatable
fn default_current_repeatable() -> usize {
	u8::max_value() as usize
}
