use serde::{Deserialize, Serialize};

/// byte shifts for a u64 ("groups" of bits)
#[rustfmt::skip]
macro_rules! group {
	(1, $n:expr) => { $n       };
	(2, $n:expr) => { $n << 8  };
	(3, $n:expr) => { $n << 16 };
	(4, $n:expr) => { $n << 24 };
	(5, $n:expr) => { $n << 32 };
	(6, $n:expr) => { $n << 40 };
	(7, $n:expr) => { $n << 48 };
	(8, $n:expr) => { $n << 56 };
}

bitflags::bitflags! {
	/// Settings used for a channel
	#[derive(Serialize, Deserialize)]
	pub struct Flags: u64 {
		/// Literally nothing (always present)
		const NOTHING     = group!(1, 0b0000_0000);
		/// Channel is in abuse mode
		const ABUSE       = group!(1, 0b0000_0001);
		/// Disable conservative ratelimiting and live on the edge
		const NO_LIMIT    = group!(2, 0b0000_0001);
	}
}

impl Default for Flags {
	fn default() -> Flags {
		Flags::NOTHING
	}
}

bitflags::bitflags! {
	/// Twitch Message Flags
	pub struct  MessageFlags: u8 {
		/// Nothing (always present)
		const NOTHING     = 0b0000_0000;
		/// The message contains bits
		const BITS        = 0b0000_0001;
		/// The user is mojo
		const MOJO        = 0b0000_0010;
		/// The user is a subscriber
		const SUBSCRIBER  = 0b0001_0000;
		/// The user is a moderator
		const MODERATOR   = 0b0010_0000;
		/// The user is the broadcaster
		const BROADCASTER = 0b0100_0000;
		/// The user is the owner
		const OWNER       = 0b1000_0000;
	}
}

impl Default for MessageFlags {
	fn default() -> MessageFlags {
		MessageFlags::NOTHING
	}
}

impl MessageFlags {
	/// Checks if the message creator is a subscriber
	pub fn is_subscriber(self) -> bool {
		self.contains(Self::SUBSCRIBER)
	}

	/// Checks if the message creator is a moderator
	pub fn is_moderator(self) -> bool {
		self.contains(Self::MODERATOR)
	}

	/// Checks if the message creator is a broadcaster
	pub fn is_broadcaster(self) -> bool {
		self.contains(Self::BROADCASTER)
	}

	/// Checks if the message creator is a owner
	pub fn is_owner(self) -> bool {
		self.contains(Self::OWNER)
	}

	/// Checks if the message creator is Mojo
	pub fn is_mojo(self) -> bool {
		self.contains(Self::MOJO)
	}

	/// Checks if the message creator is one of the following:
	/// - Owner
	/// - Broadcaster
	/// - Moderator
	pub fn is_privileged(self) -> bool {
		self.intersects(Self::OWNER | Self::BROADCASTER | Self::MODERATOR)
	}

	/// Checks if the message creator is one of the following:
	/// - Owner
	/// - Broadcaster
	pub fn is_high_privileged(self) -> bool {
		self.intersects(Self::OWNER | Self::BROADCASTER)
	}
}
