use serde::{Deserialize, Serialize};
use std::{io, path::Path};
use tokio::{
	fs::File,
	io::{AsyncReadExt, AsyncWriteExt},
};

/// Global bot configuration
#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Cfg {
	/// Bot-specific information
	pub bot: Bot,
	/// App specific info
	pub app: App,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Bot {
	/// Username of the bot
	pub user: String,
	/// oauth token to authenticate with (it *has* to start with `oauth:`)
	pub token: String,
	/// List of channels that should be joned
	pub channels: Vec<String>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct App {
	/// App Id for client Requests
	pub client_id: String,
	/// App Secret for client Requests
	pub client_secret: String,
}

impl Cfg {
	/// Load the configuration from the given path, creating it if it doesnt
	/// exist
	#[tracing::instrument]
	pub async fn load_or_create<P: AsRef<Path> + std::fmt::Debug>(
		path: P,
	) -> io::Result<Option<Cfg>> {
		tracing::trace!("attempting to load config");
		Self::_load_or_create(path.as_ref()).await
	}

	async fn _load_or_create(path: &Path) -> io::Result<Option<Cfg>> {
		if !path.exists() {
			tracing::trace!("missing config");
			let new = Cfg::default();
			let data = match tokio::task::spawn_blocking(move || toml::to_string(&new))
				.await
				.expect("task spawn failed")
			{
				Ok(data) => data,
				Err(why) => {
					tracing::error!("fatal error serializing default config!: {:?}", why);
					return Err(io::Error::new(io::ErrorKind::InvalidData, why));
				}
			};

			let mut file = File::create(path).await?;
			file.write_all(data.as_bytes()).await?;
			return Ok(None);
		}

		let mut file = File::open(path).await?;
		tracing::trace!(?file);
		let mut data = Vec::with_capacity(
			file.metadata().await.map(|meta| meta.len() as usize).unwrap_or(2048),
		);
		file.read_to_end(&mut data).await?;

		match toml::from_slice(&data) {
			Ok(config) => Ok(Some(config)),
			Err(why) => {
				tracing::error!("failed to load config: {:?}", why);
				Err(io::Error::new(io::ErrorKind::InvalidData, why))
			}
		}
	}
}
